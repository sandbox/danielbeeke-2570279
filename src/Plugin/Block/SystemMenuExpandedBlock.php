<?php

/**
 * @file
 * Contains \Drupal\menu_block_expanded\Plugin\Block\SystemMenuBlock.
 */

namespace Drupal\menu_block_expanded\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Plugin\Block\SystemMenuBlock as SystemMenuBlockOriginal;

/**
 * Provides a generic Menu block.
 *
 * @Block(
 *   id = "system_menu_expanded_block",
 *   admin_label = @Translation("Menu (optionaly expanded)"),
 *   category = @Translation("Menus (optionaly expanded)"),
 *   deriver = "Drupal\system\Plugin\Derivative\SystemMenuBlock"
 * )
 */
class SystemMenuExpandedBlock extends SystemMenuBlockOriginal {

  /**
  /**
   * {@inheritdoc}
   */
  public function build() {
    $menu_name = $this->getDerivativeId();
    $parameters = $this->menuTree->getCurrentRouteMenuTreeParameters($menu_name, TRUE);

    // Adjust the menu tree parameters based on the block's configuration.
    $level = $this->configuration['level'];
    $depth = $this->configuration['depth'];
    $parameters->setMinDepth($level);
    // When the depth is configured to zero, there is no depth limit. When depth
    // is non-zero, it indicates the number of levels that must be displayed.
    // Hence this is a relative depth that we must convert to an actual
    // (absolute) depth, that may never exceed the maximum depth.
    if ($depth > 0) {
      $parameters->setMaxDepth(min($level + $depth - 1, $this->menuTree->maxDepth()));
    }

    $tree = $this->menuTree->load($menu_name, $parameters);
    $manipulators = array(
      array('callable' => 'menu.default_tree_manipulators:checkAccess'),
      array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort'),
    );
    $tree = $this->menuTree->transform($tree, $manipulators);

    return $this->menuTree->build($tree);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'level' => 1,
        'depth' => 0,
        'expand_all' => 0
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->configuration;

    $form = parent::blockForm($form, $form_state);

    $form['menu_levels']['expand_all'] = array(
        '#type' => 'checkbox',
        '#title' => $this->t('Expand all children'),
        '#default_value' => $config['expand_all'],
        '#description' => $this->t("The menu's children will all be expanded, if they are shown by other configuration."),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['level'] = $form_state->getValue('level');
    $this->configuration['depth'] = $form_state->getValue('depth');
    $this->configuration['expand_all'] = $form_state->getValue('expand_all');
  }

}

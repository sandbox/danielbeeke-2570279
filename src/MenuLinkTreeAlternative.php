<?php

/**
 * @file
 * Contains \Drupal\Core\Menu\MenuLinkTree.
 */

namespace Drupal\menu_block_expanded;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Controller\ControllerResolverInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Menu\MenuLinkTree;
use Drupal\Core\Menu\MenuTreeParameters;

/**
 * Implements the loading, transforming and rendering of menu link trees.
 */
class MenuLinkTreeAlternative extends MenuLinkTree {

  /**
   * {@inheritdoc}
   */
  public function getCurrentRouteMenuTreeParameters($menu_name, $expanded = FALSE) {
    $active_trail = $this->menuActiveTrail->getActiveTrailIds($menu_name);
    $parameters = new MenuTreeParameters();

    if (!$expanded) {
      $parameters->setActiveTrail($active_trail)
        // We want links in the active trail to be expanded.
        ->addExpandedParents($active_trail)
        // We marked the links in the active trail to be expanded, but we also
        // want their descendants that have the "expanded" flag enabled to be
        // expanded.
        ->addExpandedParents($this->treeStorage->getExpanded($menu_name, $active_trail));
    }

    return $parameters;
  }
}

# Description
This module makes it possible to create a menu block where all it's children are shown expanded.

# How to use
- Install the module
- Add the contents of menu_block_expanded.services.yml to your services.yml